---
name: Table of contents
---

# Indhold

1. Introduktion
   - Hvad er it-sikkerhed ?
   - Den studerendes forudsætninger
   - Hvordan lærer du bedst ?
   - Hvem arbejder med it-sikkerhed ?
   - Hvordan holder du dig opdateret ?
   - Kommunikation
2. Versionstyring og projektstyring
   - Versions styring med GIT
   - Devops platforme
   - Projektstyrings metoder
   - Projektstyrings værktøjer
   - Personlig portfolio
3. Programmering
   - Hvorfor skal du kunne programmere ?
   - Python
   - C#
4. Scripting
   - Kommando linie værktøjer
   - Bash
   - Powershell
5. Networking
   - OSI model
   - TCP/IP
   - Porte
   - Pakker
   - Analyse af netværkstrafik
6. Kali
   - Hvad er Kali og alternativer
   - Kali værktøjs overblik
   - Penetration testing
